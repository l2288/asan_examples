# Address Sanitizer Example

- [AddressSanitizer Doc](https://clang.llvm.org/docs/AddressSanitizer.html)
- [AddressSanitizer: A Fast Address Sanity Checker](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/37752.pdf)
- [Slides, Talk](https://www.usenix.org/conference/atc12/technical-sessions/presentation/serebryany)

## Impl

- [Instrumentation](https://github.com/llvm/llvm-project/blob/main/llvm/lib/Transforms/Instrumentation/AddressSanitizer.cpp)
- [Runtime Library](https://github.com/llvm/llvm-project/tree/main/compiler-rt/lib/asan)

