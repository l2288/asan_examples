#include <thread>
#include <vector>
#include <mutex>
#include <iostream>
#include <string>

////////////////////////////////////////////////////////////////////////////////

void BadStringView() {
  std::string_view str_view;
  {
    std::string test = "Hello!";
    str_view = test;
  } // test dtor
  std::cout << str_view << std::endl;
}

////////////////////////////////////////////////////////////////////////////////

class ForwardList {
  struct Node {
    Node* next;
  };

 public:
  void Push() {
    Node* new_node = new Node{head_};
    head_ = new_node;
  }

  void Pop() {
    Node* head = head_;
    head_ = head->next;
    delete head;
  }

 private:
  Node* head_{nullptr};
};

void BrokenList() {
  ForwardList list;
  std::mutex mutex;

  // Just works

  list.Push();
  list.Push();
  list.Pop();
  list.Pop();
  list.Push();
  list.Pop();

  // Concurrent access

  std::vector<std::thread> threads;

  for (size_t i = 0; i < 5; ++i) {
    threads.emplace_back([&]() {
      for (size_t k = 0; k < 100500; ++k) {
        //std::lock_guard guard(mutex);
        list.Push();
        list.Pop();
      }
    });
  }

  for (auto& t : threads) {
    t.join();
  }
}

////////////////////////////////////////////////////////////////////////////////

int main() {
  //BadStringView();
  BrokenList();
  return 0;
}
